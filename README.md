# Joel - March 3 2019

## Installation
This repo contains two projects:

- React UI in [app/](app/)  
- Mocked php API in [api/](api/) and [server/](server/)  

### Development:
- move the repo to a php accessible directory
- install the app dependencies `cd app && npm i`
- build the app `npm run build` or live reload it `npm start` (from the `app` directory)

### Deployment:
- build the app `cd app && npm run build`
- copy the following files/directories to where you plan to serve content
    - `index.php`
    - `dist/`
    - `api/` (should remove reset.php)
    - `server/` (should not be accessible externally)
- if you are using your own server the app can be configured via `window.initialState` as done in [index.php](index.php)
```json
{
  "query": "",
  "documents": [],
  "maxFileSize": 1000, // bytes
  "accept": "image/png,image/jpeg",
  "acceptExt": "png,jpg,jpeg",
  "alert": "file uploads currently disabled"
}
```

## Security
### Cross Site Scripting (XSS): Addressed
By default when using ReactDOM and JSX, all values embedded in JSX are escaped before rendering which helps prevent against XSS.

The App also avoids other commons pitfalls such as:

- using dangerouslySetInnerHTML
- using href props on an `<a>` element (without first being validated to be safe)
- using inline styles

*Note: The api uses a php session to mock persistance. This gives the client a cookie which is not conducive to protecting against XSS. With a proper API implementation this wouldn't be a concern.*

### Cross Site Request Forgery (CSRF): Not Addressed
To protect against CSRF requires both server and UI considerations. Due to a largely mocked API this has not been accounted for. However the following could be added to address this:

- adding a server defined (once per session) secure random token that can be included as a hidden field in forms, or as a header/parameter for AJAX calls
- check the `referrer` header in the API to ensure requests only come from the app
- ensure CORS is properly configured on the server
- add a nonce to each request and ensure subsequent requests the nonce is larger

### Unrestricted File Upload: Partially Addressed
To protect against this largely relies on mitigations on the server. However a few basic things are accounted for:

- validating file size, extension and MIME type on the client and server

While these are not accounted for:

- saving the file somewhere safe (where a script/injected code couldn't run)
- protecting against double extension (ex `file.php.png`)
- protection against case sensitivity (ex `file.pHp`)
- using a file processing library to ensure file is as expected (size/type) (ex `getimagesize()`)
- checking `Content-Type` and other headers
- protecting against special filenames (ex `..`)
- whitelist for filename / filename sanitization (ex removing all control characters, special characters, unicode characters. filename max length)
- ensuring minimum file size

### Libraries
All libraries are locked in the package.json. This was done to prevent unintentional upgrades when being installed or deployed. None of the packages are (currently) vulnerable according to `npm audit`. Care should be taken to ensure this is regularly maintained.

## Improvements
- proper api with security concerns addressed
- data validation on api responses. Currently they can set the app state directly
- to the initialState defined by the server. Make this an explicit class for type safety
- more explicit type checking / Typescript
- tests
    - More for App in regards to mocking server responses / state
    - for utils/request
    - 100% coverage

## Libraries
### App
- [lodash.debounce](https://www.npmjs.com/package/lodash.debounce) to prevent repeatedly calling the API when searching
- [prop-types](https://github.com/facebook/prop-types) for runtime prop type checking
- [React](https://reactjs.org/) app framework
- [ReactDOM](https://reactjs.org/docs/react-dom.html) for rendering the app in the DOM
### Development
- [Webpack](https://webpack.js.org/) to bundle the app
    - `babel-loader` to use babel
    - `css-loader` to interpret `@import` and `url()` in styles
    - `sass-loader` to load sass and compile to css
    - `style-loader` to inject a style element in the head 
    - `webpack-cli` for using webpack from npm scripts
- [Babel](https://babeljs.io/) to compile the app to older versions of ecmascript
    - `@babel/preset-env` for using the latest version of ecmascript
    - `@babel/preset-react` for using React/JSX
- [Sass](https://sass-lang.com/) via [node-sass](https://github.com/sass/node-sass) to improve style development
- [Jest](https://jestjs.io/) test framework
    - `babel-jest` to compile the app for use in tests
    - `react-test-renderer` to run snapshot tests
- [Enzyme](https://airbnb.io/enzyme/) react test utility
    - `enzyme-adapter-react-16` to use react 16.x specific hooks
- [eslint](https://eslint.org/) for linting javascript
    - `eslint-config-airbnb` Airbnb defualt styleguide
    - `eslint-plugin-import` to support es6+
    - `eslint-plugin-jsx-a11y` to check for accessibility issues
    - `eslint-plugin-react` to support react/jsx
- [sass-lint](https://github.com/sasstools/sass-lint) for linting styles


## API
By and large the API is mocked. It uses a session to mock persistance.

Documents are of the form:
```json
{
  "id": "123-abc",
  "name": "Document 1",
  "size": 123456 // in bytes
}
```

### GET /api/search
This endpoint is used for searching documents by name.

- accepts a query parameter `query` representing the text to match
- returns an array of documents with names matching the query as `application/json`

### POST /api/upload
This endpoint is used for uploading a document.

- accepts a single png or jpg file uploaded via `multipart/form-data` with a name of `file`
- on success returns the document uploaded as `application/json`
- on failure returns a 500 with an optional error message as `text/plain`

### DELETE /api/delete
This endpoint is used for deleting a document.

- accepts a query parameter `id` representing the document id to delete
- on success returns nothing
- on failure returns a 500 with error message as `text/plain`

### GET /api/reset
This endpoint should not be used in production. It is a helper for dealing with the mocked api session and reseting to default data state.

- accepts nothing
- returns nothing
- redirects to parent directory (index of app)

## Tests
Tests are run via the [Jest](https://jestjs.io/) framework and use the [Enzyme](https://airbnb.io/enzyme/) utility. They live within [app/tests/](app/tests/) and should follow a matching directory structure of [src/](src/)

```sh
# all tests
npm run test
# with code coverage
npm run test:coverage
# individual file
npm run test:file -- path/to/test/file.test.jsx
```

## Linting
Linting is done via [eslint](https://eslint.org/) for javascript and [sass-lint](https://github.com/sasstools/sass-lint) for sass. This in combination with [EditorConfig](https://editorconfig.org/) helps ensure a consistent codebase. Please run these before commiting to this repo:

```sh
npm run lint:js
npm run lint:css
```
