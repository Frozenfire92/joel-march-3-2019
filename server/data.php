<?php
  session_start();

  // We save these on the session to mock persistance
  if (!isset($_SESSION['documents'])) {
    $_SESSION['documents'] = array(
      (object) [
        'id' => '123-abc',
        'name' => 'Document 1.png',
        'size' => 307200
      ],
      (object) [
        'id' => '456-yex',
        'name' => 'This file has a very long name that could be problematic.jpg',
        'size' => 614400
      ],
      (object) [
        'id' => '766-asdf',
        'name' => 'photo.jpeg',
        'size' => 921600
      ],
      (object) [
        'id' => '876-asd',
        'name' => 'Document 4 TODO special character',
        'size' => 1258291
      ],
      (object) [
        'id' => 'vcx-123',
        'name' => 'very cool',
        'size' => 1000
      ],
      (object) [
        'id' => 'asd-567',
        'name' => 'file1234.jpg',
        'size' => 9646899
      ]
    );
  }

  $maxFileSize = 10485760; // 10MB
  $accept = 'image/jpeg,image/png';
  $acceptExt = 'png,jpg,jpeg';
  $alert = ''; // 'Server unable to upload at the moment.';
?>
