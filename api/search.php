<?php
  include('../server/data.php');

  if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    header('Content-Type: application/json');
    $result = new StdClass();
    try {
      $result->documents = array_values(array_filter($_SESSION['documents'], function ($e) {
        return strpos(strtolower($e->name), strtolower($_GET['query'])) > -1;
      }));
    } catch (Exception $e) {
      $result->documents = array();
    }
    $response = json_encode($result);
    echo $response;
  }
  else {
    http_response_code(405);
    header('allow: GET');
  }
?>
