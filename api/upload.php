<?php
  ini_set('file_uploads', 'On');
  ini_set('post_max_size', '10M');
  ini_set('upload_max_filesize', '10M');
  ini_set('max_file_uploads', 1);

  include('../server/data.php');

  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (is_uploaded_file($_FILES['file']['tmp_name'])) {
      $success = True;
      $error = '';
      // file size
      if ($_FILES["file"]["size"] > $maxFileSize) {
        $error = 'File too large';
        $success = False;
      }

      // file extension
      $path = strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));
      $validExtensions = ['jpg', 'jpeg', 'png'];
      $hmm = $_FILES['file']['name'];
      if (!in_array($path, $validExtensions)) {
        $error = "Invalid file type";
        $success = False;
      }

      // file mime
      $mimetype = mime_content_type($_FILES['file']['tmp_name']);
      $validMIMEs = ['image/jpeg', 'image/png'];
      if(!in_array($mimetype, $validMIMEs)) {
        $error = "Invalid file type";
        $success = False;
      }

      // Check if $uploadOk is set to 0 by an error
      if ($success) {
        header('Content-Type: application/json');
        $response = new StdClass();
        $response->id = uniqid();
        $response->name = $_FILES['file']['name'];
        $response->size = $_FILES['file']['size'];
        array_push($_SESSION['documents'], $response);
        echo json_encode($response);
      } else {
        header('Content-Type: text/plain');
        http_response_code(500);
        die($error);
      }
    } else {
      header('Content-Type: text/plain');
      http_response_code(500);
      die();
    }
  }
  else {
    http_response_code(405);
    header('allow: POST');
  }
?>
