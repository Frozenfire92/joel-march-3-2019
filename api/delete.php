<?php
  include('../server/data.php');

  if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    try {
      $_SESSION['documents'] = array_values(array_filter($_SESSION['documents'], function($e) {
        return $e->id != $_GET['id'];
      }));
    } catch (Exception $e) {
      header('Content-Type: text/plain');
      http_response_code(500);
      die('couldn\'t delete file');
    }
  }
  else {
    http_response_code(405);
    header('allow: DELETE');
  }
?>
