import readableBytes from '../../src/utils/readable-bytes';

describe('readable-bytes', () => {
  test('it works for bytes', () => {
    expect(readableBytes(0)).toBe('0b');
    expect(readableBytes(1023)).toBe('1023b');
  });

  test('it works for kilobytes', () => {
    expect(readableBytes(1024)).toBe('1.00kB');
    expect(readableBytes(1048575)).toBe('1024.00kB');
  });

  test('it works for megabytes', () => {
    expect(readableBytes(1048576)).toBe('1.00MB');
    expect(readableBytes(1073741823)).toBe('1024.00MB');
  });

  test('it works for gigabytes', () => {
    expect(readableBytes(1073741824)).toBe('1.00GB');
    expect(readableBytes(107374182400)).toBe('100.00GB');
  });
});
