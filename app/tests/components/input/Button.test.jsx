import React from 'react';
import renderer from 'react-test-renderer';
import {
  configure,
  shallow,
  mount,
  render,
} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Button from '../../../src/components/input/Button';

configure({ adapter: new Adapter() });

describe('Button', () => {
  test('snapshot | no props', () => {
    const component = renderer.create(<Button />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('snapshot | with children', () => {
    const component = renderer.create(<Button>Click me!</Button>);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('snapshot | with label', () => {
    const component = renderer.create(<Button label="Click here" />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('snapshot | with onClick', () => {
    const onClick = () => {};
    const component = renderer.create(<Button onClick={onClick} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('DOM | should mount in full DOM', () => {
    expect(mount(<Button />).find('button').length).toBe(1);
  });

  test('DOM | should render to static HTML', () => {
    expect(render(<Button />).text()).toEqual('Click');
  });

  test('DOM | should have type attribute of button', () => {
    expect(shallow(<Button />).find('button[type="button"]').length).toBe(1);
  });

  test('DOM | works when onClick not passed in', () => {
    const button = shallow(<Button />);
    button.find('button').simulate('click');
    expect(button.exists()).toBe(true);
  });

  test('DOM | onClick should be called on click', () => {
    const onClick = jest.fn();
    const button = shallow(<Button onClick={onClick} />);
    button.find('button').simulate('click');
    expect(onClick.mock.calls.length).toBe(1);
  });
});
