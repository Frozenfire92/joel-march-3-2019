import React from 'react';
import renderer from 'react-test-renderer';
import {
  configure,
  shallow,
  mount,
  render,
} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Text from '../../../src/components/input/Text';

configure({ adapter: new Adapter() });

describe('Text', () => {
  test('snapshot | no props', () => {
    const component = renderer.create(<Text />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('snapshot | with value', () => {
    const component = renderer.create(<Text value="my query" />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('snapshot | with placeholder', () => {
    const component = renderer.create(<Text placeholder="Search..." />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('snapshot | with value and placeholder', () => {
    const component = renderer.create(<Text value="my query" placeholder="Search..." />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('DOM | should mount in full DOM', () => {
    expect(mount(<Text />).find('input').length).toBe(1);
  });

  test('DOM | should render to static HTML', () => {
    expect(render(<Text />).is('input')).toBe(true);
  });

  test('DOM | should have proper attributes', () => {
    const component = shallow(<Text value="my query" placeholder="Search..." />);
    expect(component.is('input[type="text"]')).toBe(true);
    expect(component.is('[value="my query"]')).toBe(true);
    expect(component.is('[placeholder="Search..."]')).toBe(true);
  });

  test('DOM | works when onChange not passed in', () => {
    const component = shallow(<Text />);
    component.simulate('change', { target: { value: 'wow' } });
    expect(component.exists()).toBe(true);
  });

  test('DOM | should call onChange on change', () => {
    const onChange = jest.fn();
    const component = shallow(<Text onChange={onChange} />);
    component.simulate('change', { target: { value: 'wow' } });
    expect(onChange.mock.calls.length).toBe(1);
    expect(onChange.mock.calls[0][0]).toBe('wow');
  });
});
