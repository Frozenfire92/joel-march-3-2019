import React from 'react';
import renderer from 'react-test-renderer';
import {
  configure,
  shallow,
  mount,
  render,
} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

// Renamed not to conflict with File constructor for simulating file input
import FileInput from '../../../src/components/input/File';

configure({ adapter: new Adapter() });

describe('File', () => {
  test('snapshot | no props', () => {
    const component = renderer.create(<FileInput />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('snapshot | with accept', () => {
    const component = renderer.create(<FileInput />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('snapshot | with label', () => {
    const component = renderer.create(<FileInput label="upload file" />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('snapshot | with accept and label', () => {
    const component = renderer.create(<FileInput accept="image/png" />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('DOM | should mount in full DOM', () => {
    const component = mount(<FileInput />);
    expect(component.find('input').length).toBe(1);
  });

  test('DOM | should render to static HTML', () => {
    const component = render(<FileInput />);
    expect(component.find('input').length).toBe(1);
  });

  test('DOM | should have proper attributes', () => {
    const component = shallow(<FileInput accept="image/png" />);
    expect(component.hasClass('button')).toBe(true);
    expect(component.find('input').is('[type="file"]')).toBe(true);
    expect(component.find('input').is('[accept="image/png"]')).toBe(true);
  });

  test('DOM | works when onChange not passed in', () => {
    const component = shallow(<FileInput />);
    component.find('input').simulate('change', {
      target: {
        files: [
          new File([''], 'filename'),
        ],
      },
    });
    expect(component.exists()).toBe(true);
  });

  test('DOM | onChange should be called on change', () => {
    const onChange = jest.fn();
    const component = shallow(<FileInput onChange={onChange} />);
    component.find('input').simulate('change', {
      target: {
        files: [
          new File([''], 'filename'),
        ],
      },
    });
    expect(onChange.mock.calls.length).toBe(1);
  });

  test('DOM | should block improper file types', () => {
    const onChange = jest.fn();
    const onError = jest.fn();
    const component = shallow(<FileInput
      accept="image/png"
      onChange={onChange}
      onError={onError}
    />);
    const input = component.find('input');
    input.simulate('change', {
      target: {
        files: [
          new File([''], 'filename.png', { type: 'image/png' }),
        ],
      },
    });
    input.simulate('change', {
      target: {
        files: [
          new File([''], 'filename.jpg', { type: 'image/jpeg' }),
        ],
      },
    });
    expect(onChange.mock.calls.length).toBe(1);
    expect(onError.mock.calls.length).toBe(1);
  });

  test('DOM | should block improper file extensions', () => {
    const onChange = jest.fn();
    const onError = jest.fn();
    const component = shallow(<FileInput
      accept="image/png"
      acceptExt="png"
      onChange={onChange}
      onError={onError}
    />);
    const input = component.find('input');
    input.simulate('change', {
      target: {
        files: [
          new File([''], 'filename.pong', { type: 'image/png' }),
        ],
      },
    });
    expect(onChange.mock.calls.length).toBe(0);
    expect(onError.mock.calls.length).toBe(1);
  });

  test('DOM | should block large files', () => {
    const onChange = jest.fn();
    const onError = jest.fn();
    const component = shallow(<FileInput
      maxSize={1}
      onChange={onChange}
      onError={onError}
    />);
    const input = component.find('input');
    input.simulate('change', {
      target: {
        files: [
          new File([''], 'filename.png'),
        ],
      },
    });
    input.simulate('change', {
      target: {
        files: [
          new File(['wow'], 'filename.jpg'),
        ],
      },
    });
    expect(onError.mock.calls.length).toBe(1);
    expect(onChange.mock.calls.length).toBe(1);
  });

  test('DOM | should block improper file types with no onError passed in', () => {
    const onChange = jest.fn();
    const component = shallow(<FileInput
      accept="image/png"
      onChange={onChange}
    />);
    const input = component.find('input');
    input.simulate('change', {
      target: {
        files: [
          new File([''], 'filename.jpg', { type: 'image/jpeg' }),
        ],
      },
    });
    expect(onChange.mock.calls.length).toBe(0);
  });

  test('DOM | should block improper file extensions with no onError passed in', () => {
    const onChange = jest.fn();
    const component = shallow(<FileInput
      accept="image/png"
      acceptExt="png"
      onChange={onChange}
    />);
    const input = component.find('input');
    input.simulate('change', {
      target: {
        files: [
          new File([''], 'filename.pong', { type: 'image/png' }),
        ],
      },
    });
    expect(onChange.mock.calls.length).toBe(0);
  });

  test('DOM | should block large files with no onError passed in', () => {
    const onChange = jest.fn();
    const component = shallow(<FileInput
      maxSize={1}
      onChange={onChange}
    />);
    const input = component.find('input');
    input.simulate('change', {
      target: {
        files: [
          new File(['wow'], 'filename.jpg'),
        ],
      },
    });
    expect(onChange.mock.calls.length).toBe(0);
  });
});
