import React from 'react';
import renderer from 'react-test-renderer';
import {
  configure,
  shallow,
  mount,
  render,
} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import DocumentCard from '../../src/components/DocumentCard';

configure({ adapter: new Adapter() });

const mockDocument = {
  id: '123',
  name: 'image.png',
  size: 10000,
};

describe('DocumentCard', () => {
  test('snapshot | no props', () => {
    const component = renderer.create(<DocumentCard />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('snapshot | with document', () => {
    const component = renderer.create(<DocumentCard document={mockDocument} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('DOM | should mount in full DOM', () => {
    expect(mount(<DocumentCard />).find('.document-card').length).toBe(1);
  });

  test('DOM | should render to static HTML', () => {
    expect(render(<DocumentCard />).is('.document-card')).toBe(true);
  });

  test('DOM | should handle delete button click', () => {
    const deleteDocument = jest.fn();
    const component = shallow(<DocumentCard
      document={mockDocument}
      deleteDocument={deleteDocument}
    />);
    component.find('Button').simulate('click');
    expect(deleteDocument.mock.calls.length).toBe(1);
    expect(deleteDocument.mock.calls[0][0]).toBe(mockDocument);
  });
});
