import React from 'react';
import renderer from 'react-test-renderer';
import {
  configure,
  shallow,
  mount,
  render,
} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Alert from '../../src/components/Alert';

configure({ adapter: new Adapter() });

describe('Alert', () => {
  test('snapshot | no props', () => {
    const component = renderer.create(<Alert />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('snapshot | with message', () => {
    const component = renderer.create(<Alert message="some message" />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('DOM | should mount in full DOM', () => {
    expect(mount(<Alert />).find('.alert').length).toBe(1);
  });

  test('DOM | should render to static HTML', () => {
    expect(render(<Alert />).is('.alert')).toBe(true);
  });

  test('DOM | should handle ok button click', () => {
    const ok = jest.fn();
    const component = shallow(<Alert ok={ok} />);
    component.find('Button').simulate('click');
    expect(ok.mock.calls.length).toBe(1);
  });
});
