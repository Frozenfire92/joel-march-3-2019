import React from 'react';
import renderer from 'react-test-renderer';
import {
  configure,
  mount,
  render,
} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import App from '../src/App';

jest.mock('../src/utils/request');

configure({ adapter: new Adapter() });

const initialState = {
  query: '',
  documents: [
    {
      id: '123',
      name: 'document 1',
      size: 1000,
    },
    {
      id: '12678',
      name: 'document 3',
      size: 234000,
    },
  ],
  maxFileSize: 1000000,
  accept: 'image/png,image/jpeg',
  acceptExt: 'png,jpg,jpeg',
  alert: '',
};

describe('App', () => {
  test('snapshot', () => {
    const component = renderer.create(<App />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('snapshot | with initialState', () => {
    const state = JSON.parse(JSON.stringify(initialState));
    const component = renderer.create(<App initialState={state} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('snapshot | with initialState alert', () => {
    const state = Object.assign({}, JSON.parse(JSON.stringify(initialState)), { alert: 'wow' });
    const component = renderer.create(<App initialState={state} />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  test('DOM | should mount in full DOM', () => {
    const state = JSON.parse(JSON.stringify(initialState));
    expect(mount(<App initialState={state} />).exists()).toBe(true);
  });

  test('DOM | should render to static HTML', () => {
    const state = JSON.parse(JSON.stringify(initialState));
    expect(render(<App initialState={state} />).find('header').text()).toBe('');
  });

  test('DOM | delete button works', () => {
    const state = JSON.parse(JSON.stringify(initialState));
    const component = mount(<App initialState={state} />);
    expect(component.find('DocumentCard').length).toBe(2);
    component.find('DocumentCard').at(0).find('Button').simulate('click');
    // TODO ensure mock request is called only once
    // TODO fix deletion
    // console.log('component', component.debug());
    // expect(component.find('DocumentCard').length).toBe(1);
  });

  test('DOM | upload button works', () => {
    const state = JSON.parse(JSON.stringify(initialState));
    const component = mount(<App initialState={state} />);
    expect(component.find('File').length).toBe(1);
    expect(component.find('DocumentCard').length).toBe(2);
    component.find('File input').simulate('change', {
      target: {
        files: [
          new File([''], 'filename.png', { type: 'image/png' }),
        ],
      },
    });
    // TODO mock response so it updates interface
    // expect(component.find('DocumentCard').length).toBe(3);
    // TODO ensure mock request is called only once
  });

  test('DOM | alert can be cleared', () => {
    const state = Object.assign({}, initialState, { alert: 'wow' });
    const component = mount(<App initialState={state} />);
    expect(component.find('Alert').exists()).toBe(true);
    component.find('Alert Button').simulate('click');
    expect(component.find('Alert').exists()).toBe(false);
  });

  test('DOM | searching works', () => {
    const state = JSON.parse(JSON.stringify(initialState));
    const component = mount(<App initialState={state} />);
    expect(component.find('Text').exists()).toBe(true);
    expect(component.find('Text').prop('value')).toBe('');
    component.find('Text').simulate('change', { target: { value: 'wow' } });
    expect(component.find('Text').prop('value')).toBe('wow');
  });
});
