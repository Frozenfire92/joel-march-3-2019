import React from 'react';
import { render } from 'react-dom';

import App from './App';

render(<App initialState={window.initialState} />, document.getElementById('app'));
