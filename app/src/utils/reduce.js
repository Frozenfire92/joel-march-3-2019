export const sum = (m, n) => m + n;

export default {
  sum,
};
