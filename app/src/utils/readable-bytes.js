const KB = 1024;
const MB = 1048576;
const GB = 1073741824;

export default (bytes) => {
  if (bytes < KB) {
    return `${bytes}b`;
  }
  if (bytes < MB) {
    return `${(bytes / KB).toFixed(2)}kB`;
  }
  if (bytes < GB) {
    return `${(bytes / MB).toFixed(2)}MB`;
  }
  return `${(bytes / GB).toFixed(2)}GB`;
};
