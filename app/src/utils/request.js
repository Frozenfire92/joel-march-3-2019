const methods = ['GET', 'POST', 'DELETE'];

export const prepareFileData = (file) => {
  const formData = new FormData();
  formData.append('file', file);
  return formData;
};

export const prepareFormData = (json) => {
  const formData = new FormData();
  Object.keys(json).forEach(key => formData.append(key, json[key]));
  return formData;
};

export default (method, url, data) => new Promise((resolve, reject) => {
  if (!method || !url) {
    reject(new Error('missing method or url'));
  }

  if (methods.includes(method)) {
    const req = new XMLHttpRequest();
    let contentType;

    // Setup event listeners
    req.addEventListener('load', () => {
      if (contentType === 'application/json') {
        try {
          resolve(JSON.parse(req.response));
        } catch (error) {
          reject(new Error('invalid json'));
        }
      }
    });
    req.addEventListener('abort', () => {
      reject(new Error('abort'));
    });
    req.addEventListener('error', () => {
      reject(new Error('error'));
    });
    req.addEventListener('readystatechange', function handler() {
      if (this.readyState === this.HEADERS_RECEIVED) {
        contentType = req.getResponseHeader('Content-Type');
        if (contentType !== 'application/json' && method !== 'DELETE') {
          req.abort();
        }
      }
      if (this.readyState === 4 && this.status === 200 && method === 'DELETE') {
        resolve();
      }
    });

    req.open(method, url);
    req.send(data);
  } else {
    reject();
  }
});
