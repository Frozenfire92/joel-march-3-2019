export const prepareFileData = () => {
  const formData = new FormData();
  formData.append('file', new File([''], 'filename.png'));
  return formData;
};

export default () => new Promise(resolve => resolve());
