export const copy = n => ({ ...n });
export const size = n => n.size;

export default {
  copy,
  size,
};
