import React from 'react';
import PropTypes from 'prop-types';

import Button from './input/Button';

import '../styles/components/Alert.scss';

const Alert = ({ message, ok }) => (
  <div className="alert">
    <div className="modal flex-column">
      <p>{message}</p>
      <Button label="Ok" onClick={ok} />
    </div>
  </div>
);

Alert.propTypes = {
  message: PropTypes.string,
  ok: PropTypes.func,
};

Alert.defaultProps = {
  message: '',
  ok: null,
};

export default Alert;
