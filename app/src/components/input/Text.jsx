import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Text extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(event) {
    const { onChange } = this.props;
    if (onChange) {
      onChange(event.target.value);
    }
  }

  render() {
    const { placeholder, value } = this.props;
    return (
      <input
        type="text"
        placeholder={placeholder}
        value={value}
        onChange={this.onChange}
      />
    );
  }
}

Text.propTypes = {
  value: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
};

Text.defaultProps = {
  value: '',
  placeholder: '',
  onChange: null,
};

export default Text;
