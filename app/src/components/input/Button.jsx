import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Button extends Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick() {
    const { onClick } = this.props;
    if (onClick) {
      onClick();
    }
  }

  render() {
    const { label, children } = this.props;
    return (
      <button onClick={this.onClick} type="button">
        {label}
        {children}
      </button>
    );
  }
}

Button.propTypes = {
  label: PropTypes.string,
  children: PropTypes.node,
  onClick: PropTypes.func,
};

Button.defaultProps = {
  label: 'Click',
  children: null,
  onClick: null,
};

export default Button;
