import React, { Component } from 'react';
import PropTypes from 'prop-types';

import readableBytes from '../../utils/readable-bytes';

class File extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(event) {
    const {
      onChange,
      onError,
      accept,
      acceptExt,
      maxSize,
    } = this.props;

    const file = event.target.files[0];

    const acceptedMIMETypes = (accept && accept.split(',')) || [];
    if (acceptedMIMETypes.length && !acceptedMIMETypes.includes(file.type)) {
      if (onError && typeof onError === 'function') {
        onError(`Invalid file type: ${file.type}. Try: ${acceptedMIMETypes.join(', ')}`);
      }
      return;
    }

    const acceptedExtensions = (acceptExt && acceptExt.split(',')) || [];
    const fileExtension = file.name.match(/\.(.*?)$/);
    if (acceptedExtensions.length
      && fileExtension
      && !acceptedExtensions.includes(fileExtension[1])
    ) {
      if (onError && typeof onError === 'function') {
        onError(`Invalid file type: ${file.type}. Try: ${acceptedMIMETypes.join(', ')}`);
      }
      return;
    }

    if (maxSize && file.size > maxSize) {
      if (onError && typeof onError === 'function') {
        onError(`File too large: ${readableBytes(file.size)} (max: ${readableBytes(maxSize)})`);
      }
      return;
    }

    if (onChange) {
      onChange(file);
    }
  }

  render() {
    const { label, accept } = this.props;
    return (
      /* eslint-disable jsx-a11y/label-has-for, jsx-a11y/label-has-associated-control */
      // for 100% a11y we should also have htmlFor and id but nesting the input is generally enough
      <label className="button">
        <input
          type="file"
          accept={accept}
          onChange={this.onChange}
        />
        {label}
      </label>
      /* eslint-enable jsx-a11y/label-has-for, jsx-a11y/label-has-associated-control */
    );
  }
}

File.propTypes = {
  label: PropTypes.string,
  accept: PropTypes.string,
  acceptExt: PropTypes.string,
  maxSize: PropTypes.number,
  onChange: PropTypes.func,
  onError: PropTypes.func,
};

File.defaultProps = {
  label: '',
  accept: '',
  acceptExt: '',
  maxSize: 0,
  onChange: null,
  onError: null,
};

export default File;
