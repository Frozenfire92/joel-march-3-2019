import React from 'react';
import PropTypes from 'prop-types';

import Button from './input/Button';

import '../styles/components/DocumentCard.scss';

import readableBytes from '../utils/readable-bytes';

const DocumentCard = ({
  document,
  deleteDocument,
}) => (
  <div className="document-card flex-column">
    <h3>{document.name}</h3>
    <div className="flex-row">
      <span>{readableBytes(document.size)}</span>
      <Button
        label="delete"
        onClick={() => deleteDocument(document)}
      />
    </div>
  </div>
);

DocumentCard.propTypes = {
  document: PropTypes.shape({
    id: PropTypes.string,
    name: PropTypes.string,
    size: PropTypes.number,
  }),
  deleteDocument: PropTypes.func,
};

DocumentCard.defaultProps = {
  document: {
    id: '',
    name: '',
    size: 100,
  },
  deleteDocument: null,
};

export default DocumentCard;
