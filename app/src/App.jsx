import React, { Component } from 'react';

import debounce from 'lodash.debounce';
import PropTypes from 'prop-types';

import './styles/app.scss';

import Alert from './components/Alert';
import DocumentCard from './components/DocumentCard';
import File from './components/input/File';
import Text from './components/input/Text';

import readableBytes from './utils/readable-bytes';
import request, { prepareFileData } from './utils/request';

import { copy, size } from './utils/map';
import { sum } from './utils/reduce';

class App extends Component {
  static calculateSize(documents) {
    return documents
      .map(size)
      .reduce(sum, 0);
  }

  static createInitialState({
    accept = '',
    acceptExt = '',
    alert = '',
    documents = [],
    maxFileSize = 0,
    query = '',
  }) {
    const allDocuments = documents.map(copy);
    const totalSize = App.calculateSize(documents);

    return {
      accept,
      acceptExt,
      alert,
      allDocuments,
      documents: documents.map(copy),
      maxFileSize,
      query,
      totalSize,
    };
  }

  constructor(props) {
    super(props);

    // Set state
    this.state = App.createInitialState(props.initialState || {});

    // Bind methods
    this.apiDelete = this.apiDelete.bind(this);
    this.apiUpload = this.apiUpload.bind(this);
    this.clearAlert = this.clearAlert.bind(this);
    this.debouncedApiSearch = debounce(this.apiSearch.bind(this), 500);
    this.search = this.search.bind(this);
    this.setAlert = this.setAlert.bind(this);
  }

  setAlert(alert) {
    this.setState({ alert });
  }

  clearAlert() {
    this.setAlert(null);
  }

  search(input) {
    this.setState(state => ({
      query: input,
      documents: input
        ? state.documents
        : state.allDocuments,
      totalSize: input
        ? state.totalSize
        : App.calculateSize(state.allDocuments),
    }));

    if (input) {
      this.debouncedApiSearch();
    }
  }

  apiDelete(document) {
    const clean = encodeURI(document.id);
    request('DELETE', `api/delete?id=${clean}`)
      .then(() => {
        const filter = documents => documents
          .filter(n => n.id !== document.id)
          .map(copy);
        this.setState(state => ({
          allDocuments: filter(state.allDocuments),
          documents: filter(state.documents),
          totalSize: state.totalSize - document.size,
        }));
      })
      .catch(() => {
        this.setState({ alert: 'unable to delete file' });
      });
  }

  apiSearch() {
    const { query } = this.state;
    if (query) {
      const clean = encodeURI(query);
      request('GET', `api/search?query=${clean}`)
        .then((response) => {
          if (response && response.documents && Array.isArray(response.documents)) {
            this.setState({
              documents: response.documents.map(copy),
              totalSize: App.calculateSize(response.documents),
            });
          } else {
            this.setState({
              documents: [],
              totalSize: 0,
            });
          }
        })
        .catch(() => {
          this.setState({ alert: 'unable to search' });
        });
    }
  }

  apiUpload(file) {
    request('POST', 'api/upload', prepareFileData(file))
      .then((response) => {
        if (response) {
          this.setState(state => ({
            allDocuments: [
              ...state.allDocuments.map(copy),
              response,
            ],
            documents: [
              ...state.documents.map(copy),
              response,
            ],
            totalSize: state.totalSize + response.size,
          }));
        }
      })
      .catch(() => {
        this.setState({ alert: 'unable to upload file' });
      });
  }

  render() {
    const {
      accept,
      acceptExt,
      alert,
      documents,
      maxFileSize,
      query,
      totalSize,
    } = this.state;
    return (
      <>
        <header className="flex-row flex-reverse">
          <div className="flex-item">
            <File
              label="UPLOAD"
              accept={accept}
              acceptExt={acceptExt}
              maxSize={maxFileSize}
              onChange={this.apiUpload}
              onError={this.setAlert}
            />
          </div>
          <div className="flex-item">
            <Text placeholder="Search documents..." onChange={this.search} value={query} />
          </div>
        </header>
        <main>
          {documents.length
            ? (
              <aside className="flex-row">
                <h2 className="flex-item">
                  {documents.length}
                  &nbsp;documents
                </h2>
                <h4 className="flex-item">
                  Total size:&nbsp;
                  {readableBytes(totalSize)}
                </h4>
              </aside>
            )
            : null
          }
          <div className="grid-container">
            {documents.map(n => (
              <div key={n.id}>
                <DocumentCard document={n} deleteDocument={this.apiDelete} />
              </div>
            ))}
          </div>
        </main>
        {alert
          ? <Alert message={alert} ok={this.clearAlert} />
          : null
        }
      </>
    );
  }
}

App.propTypes = {
  initialState: PropTypes.shape({
    query: PropTypes.string,
    documents: PropTypes.array,
    maxFileSize: PropTypes.number,
    accept: PropTypes.string,
    acceptExt: PropTypes.string,
    alert: PropTypes.string,
  }),
};

App.defaultProps = {
  initialState: null,
};

export default App;
