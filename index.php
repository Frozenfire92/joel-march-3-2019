<?php
  include('server/data.php');

  $initialState = new StdClass();
  $initialState->query = '';
  $initialState->documents = $_SESSION['documents'];
  $initialState->maxFileSize = $maxFileSize;
  $initialState->accept = $accept;
  $initialState->$acceptExt = $$acceptExt;
  $initialState->alert = $alert;
?>

<!doctype html>

<html lang="en">
<head>
  <title>File Upload</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script>
    window.initialState = <?php echo json_encode($initialState); ?>;
  </script>
</head>

<body>
  <div id="app">
    <noscript>Enable javascript to view this app</noscript>
  </div>
  <script type="text/javascript" src="dist/app.js"></script>
</body>
</html>
